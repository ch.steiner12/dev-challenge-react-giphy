/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    fontFamily: {
      Roboto: ["Roboto", "sans-serif"],
    },
    extend: {
      colors: {
        darkGray: "#212529",
        lightGray: "#6C757D",
        actionPurple: "#8E2EFF",
        middleGray: "#343A40",
      },
      container: {
        center: true,
        padding: "2rem",
      },
      content: {
        searchIcon: 'url("/src/assets/icons/find_icon.svg")',
        heartIcon: 'url("/src/assets/icons/heart_filled_white.svg")',
      },
    },
  },
  plugins: [],
};
