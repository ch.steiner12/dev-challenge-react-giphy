import React, { useEffect, useState } from "react";
import { GifElement } from "../../types";
import { ListElement } from "./ListElement";

type ListViewProps = {
  data: GifElement[];
};

export const ListView = ({ data }: ListViewProps) => {
  return (
    <>
      <section className="my-10 md:my-20 fade-in-image">
        <ul className="flex flex-wrap gap-2 justify-center">
          {/* CHECK IF ONE RANDOM GIF OR GIF-ARRAY */}
          {data.length > 1 ? (
            <>
              {data.map(({ images, id }: GifElement, index) => {
                return (
                  <ListElement url={images.original.url} key={id} id={id} />
                );
              })}
            </>
          ) : (
            <>
              <ListElement
                url={data[0].images.original.url}
                key={data[0].id}
                id={data[0].id}
              />
            </>
          )}
        </ul>
      </section>
    </>
  );
};
