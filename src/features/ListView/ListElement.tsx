import React, { useContext, useState } from "react";
import FsLightbox from "fslightbox-react";
import { GifContext } from "../../context/GifContext";

type ListElementProps = {
  url: string;
  id: string;
};

export const ListElement = ({ url, id }: ListElementProps) => {
  const { addFavoriteGif, favorites } = useContext(GifContext);
  const [toggler, setToggler] = useState(false);

  return (
    <article className="relative  h-54 md:h-72 flex-grow last:flex-grow-0 border-2 border-darkGray hover:border-actionPurple hover:cursor-pointer">
      <div
        className="listViewImageWrapper"
        onClick={() => setToggler(!toggler)}
      >
        <img src={url} className="listViewImage" />
      </div>
      {/* check if gif is already a favorite */}
      {favorites.find((element) => element === id) ? (
        <div className="absolute  w-fit top-2 right-2  bg-actionPurple p-2 rounded-full transition-all  drop-shadow-md flex text-xs items-center gap-2">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="22.727"
            height="21.548"
            viewBox="0 0 22.727 21.548"
          >
            <path
              id="iconmonstr-favorite-7"
              d="M15.545,1,10.364,4.455,5.182,1,0,5.318v6.045L10.364,20l10.364-8.636V5.318Z"
              transform="translate(1 0.246)"
              fill="#fff"
              stroke="#fff"
              strokeWidth="2"
            />
          </svg>
        </div>
      ) : (
        <div
          onClick={() => addFavoriteGif(id)}
          className="absolute  w-fit top-2 right-2 hover:scale-110 bg-actionPurple p-2 rounded-full transition-all hover:transition-all drop-shadow-md flex text-xs items-center gap-2"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="22.727"
            height="21.548"
            viewBox="0 0 22.727 21.548"
            className=""
          >
            <path
              id="iconmonstr-favorite-7"
              d="M15.545,1,10.364,4.455,5.182,1,0,5.318v6.045L10.364,20l10.364-8.636V5.318Z"
              transform="translate(1 0.246)"
              fill="none"
              stroke="#fff"
              strokeWidth="2"
            />
          </svg>
        </div>
      )}

      <FsLightbox toggler={toggler} sources={[`${url}`]} />
    </article>
  );
};
