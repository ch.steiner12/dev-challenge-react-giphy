import React, { useContext, useState } from "react";
import { GifContext } from "../../context/GifContext";
import { GifElement } from "../../types";

type FavoriteElementProp = {
  element: GifElement;
};

export const FavoriteElement = ({ element }: FavoriteElementProp) => {
  const { deleteFavoriteGif } = useContext(GifContext);

  const [deleteWindow, setDeleteWindow] = useState(false);

  return (
    <article className="relative h-54 md:h-72 select-none flex-grow last:flex-grow-0 border-2 border-darkGray hover:border-actionPurple hover:cursor-pointer">
      <div className="listViewImageWrapper">
        <img src={element.images.original.url} className="listViewImage" />
      </div>

      <div
        onClick={() => setDeleteWindow(!deleteWindow)}
        className="z-10 hover:opacity-100 transition-all hover:transition-all opacity-0 bg-actionPurple absolute top-0 left-00 w-full h-full font-bold gap-1 text-3xl flex flex-col justify-center items-center"
      >
        {/* FINAL DELETE FAVORITE FROM LIST */}
        {deleteWindow ? (
          <div className="flex justify-center font-bold bg-actionPurple w-full h-full flex-col items-center text-xs p-2 absolute top-0 left-0 z-20">
            <p className=" text-center">
              Do you really want to delete this gif from favorite list?
            </p>
            <div className="flex gap-2 mt-5">
              <button
                className="bg-white text-actionPurple rounded-md text-xl px-2 py-1 hover:opacity-80"
                onClick={() => deleteFavoriteGif(element.id)}
              >
                yes
              </button>
              <button
                className="text-xl hover:underline"
                onClick={() => setDeleteWindow(false)}
              >
                no
              </button>
            </div>
          </div>
        ) : (
          <div className="text-xs flex flex-col gap-1 items-center">
            <span className="text-xl ">x</span>
            <span>delete gif from favorites</span>
          </div>
        )}
      </div>
    </article>
  );
};
