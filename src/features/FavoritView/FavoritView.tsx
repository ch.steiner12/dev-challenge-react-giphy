import React from "react";
import { ErrorBar } from "../../common/components/ErrorBar";
import { useGetGifByID } from "../../common/hooks/useGetGifByID";
import { FavoriteElement } from "./FavoriteElement";

export const FavoritView = () => {
  const response = useGetGifByID();
  return (
    <>
      <h1>Favorites</h1>
      <section className="flex flex-wrap gap-2 my-10 md:my-20 fade-in-image">
        {/* CHECK IF THERE ARE ALREADY GIF IN FAVORITE LIST */}
        {response ? (
          <>
            {response.length <= 0 && (
              <ErrorBar content={"No favorites in here yet"} />
            )}
            {response.map((element) => {
              return (
                element.data && (
                  <FavoriteElement
                    element={element.data.data.data}
                    key={element.data.data.data.id}
                  />
                )
              );
            })}
          </>
        ) : (
          <ErrorBar content={"error displaying favorites"} />
        )}
      </section>
    </>
  );
};
