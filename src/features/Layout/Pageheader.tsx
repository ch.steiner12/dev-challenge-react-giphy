import React from "react";
import { Link } from "react-router-dom";
import { ActionButton } from "../../common/components/ActionButton";

export const Pageheader = () => {
  return (
    <header className="container my-10 flex justify-center md:justify-end gap-3 sticky top-0 py-3 z-20 bg-darkGray w-fit rounded-b-lg">
      <div className="flex gap-2 md:gap-5">
        <ActionButton
          link="/favorite"
          icon="heart"
          title="view favorite list"
        />
        <ActionButton link="/" icon="search" title="search for gif" />
      </div>
    </header>
  );
};
