import React, { ReactNode } from "react";
import { Pagefooter } from "./Pagefooter";
import { Pageheader } from "./Pageheader";

type LayoutProps = {
  children: ReactNode;
};

export const Layout = ({ children }: LayoutProps) => {
  return (
    <>
      <section className="min-h-full flex flex-col items-stretch">
        <Pageheader />
        <main className="container flex-1 mt-10 md:mt-20">{children}</main>
        <Pagefooter />
      </section>
    </>
  );
};
