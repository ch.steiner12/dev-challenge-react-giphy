import React from "react";
export const Pagefooter = () => {
  return (
    <footer className="py-5 bg-middleGray mt-10">
      <div className="container text-xs text-lightGray uppercase font-light justify-between items-center text-left flex flex-col gap-5 md:flex-row">
        <div>
          <p className="font-bold text-base">Junior Dev Challenge React</p>
          <p className="text-[0.6rem] ">created by Christoph Steiner</p>
        </div>
        <p className="flex flex-col items-center md:items-end">
          <span className="text-lightGray font-light text-xs ">
            API used for this App:
          </span>
          <a href="https://developers.giphy.com/" target="blank" className=" ">
            <span>https://developers.giphy.com/</span>
          </a>
        </p>
      </div>
    </footer>
  );
};
