import React, { useContext, useEffect, useState } from "react";
import { json } from "stream/consumers";
import { ErrorBar } from "../../common/components/ErrorBar";
import { Loader } from "../../common/components/Loader";
import { useGetGif } from "../../common/hooks/useGetGif";
import { useGetRandom } from "../../common/hooks/useGetRandom";
import { GifContext } from "../../context/GifContext";
import { ListView } from "../ListView/ListView";
import { SearchForm } from "./SearchForm";

export const SearchView = () => {
  const [displayState, setDisplayState] = useState(false);

  const {
    mutate: getGif,
    data,
    isError,
    error,
    isLoading,
    isSuccess,
  } = useGetGif();
  const {
    mutate,
    data: dataGetRandom,
    isError: isErrorGetRandom,
    error: errorGetRandom,
    isLoading: isLoadingGetRandom,
  } = useGetRandom();

  return (
    <>
      <SearchForm
        onSubmit={(values) => {
          getGif(values);
          setDisplayState(false);
        }}
        getRandomGif={() => {
          mutate();
          setDisplayState(true);
        }}
      />

      {/* CHECK WHICH ACTION IS DONE --> RANDOMVIEW or LISTGIFVIEW - switch VIEWS */}
      {displayState ? (
        <>
          {dataGetRandom && <ListView data={[dataGetRandom.data.data]} />}
          {isLoadingGetRandom && <Loader />}
          {isErrorGetRandom && (
            <ErrorBar content={JSON.stringify(errorGetRandom)} />
          )}
        </>
      ) : (
        <>
          {isLoading && <Loader />}
          {isError && <ErrorBar content={JSON.stringify(error)} />}
          {data && data.data.data.length > 0 && (
            <ListView data={data.data.data} />
          )}
          {data && data.data.data.length <= 0 && (
            <div className="mt-10 md:mt-20">
              <ErrorBar content="No Gif found" />
            </div>
          )}
        </>
      )}
    </>
  );
};
