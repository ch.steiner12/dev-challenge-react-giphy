import React from "react";
import { useForm } from "react-hook-form";
import { SearchFormHeader } from "./SearchFormHeader";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { ErrorBar } from "../../common/components/ErrorBar";

export type SearchWord = {
  searchWord: string;
  searchCount: number;
};

type SearchFormProps = {
  onSubmit: (values: SearchWord) => void;
  getRandomGif: () => void;
};

// YUP SCHEME SEARCH GIF
const searchSchema = yup
  .object()
  .shape({
    searchWord: yup.string().required("Please enter a gif searchterm"),
    searchCount: yup
      .number()
      .typeError("Please enter a gifcount to search gifs")
      .min(1, "Gif count must be greater than 0")
      .max(30, "GifCount is to high"),
  })
  .required();

export const SearchForm = ({ onSubmit, getRandomGif }: SearchFormProps) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<SearchWord>({
    resolver: yupResolver(searchSchema),
  });

  return (
    <section className="w-full mx-auto max-w-4xl ">
      <article className="flex flex-col items-center justify-center gap-10">
        {/* SearchFormHeader-Component */}
        <SearchFormHeader />

        {/* SEARCH FORM */}
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="w-full flex  gap-3 flex-col md:flex-row justify-center"
        >
          <div className="flex flex-col md:flex-row w-full gap-3">
            <input
              type="text"
              placeholder="search giphy"
              className="inputSearchGif w-full"
              {...register("searchWord")}
            />
            <input
              type="number"
              placeholder="Gif count"
              className="inputSearchGif w-full md:w-1/3"
              {...register("searchCount")}
            ></input>
          </div>
          <button
            type="submit"
            className="relative inline-flex items-center justify-center  px-16 py-5 overflow-hidden font-base text-white bg-actionPurple transition duration-300 ease-out border-2 border-actionPurple shadow-md group"
          >
            <span className="absolute inset-0 flex items-center justify-center w-full h-full text-white duration-300 -translate-x-full bg-actionPurple group-hover:translate-x-0 ease">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="30.652"
                height="30.898"
                viewBox="0 0 30.652 30.898"
                className="w-6 h-6"
              >
                <path
                  id="iconmonstr-magnifier-2"
                  d="M30.652,27.867l-7.988-7.988a12.534,12.534,0,1,0-3.073,2.988L27.621,30.9ZM3.674,12.527a8.854,8.854,0,1,1,8.854,8.854A8.864,8.864,0,0,1,3.674,12.527Z"
                  transform="translate(0)"
                  fill="#fff"
                />
              </svg>
            </span>
            <span className="absolute flex items-center font-bold justify-center w-full h-full text-white transition-all duration-300 transform group-hover:translate-x-full ease">
              search
            </span>
          </button>
        </form>
      </article>

      {/* YUP HANDLE FORM ERRORS  */}
      <>
        {(errors.searchCount || errors.searchWord) && (
          <div className="flex flex-col gap-3 md:flex-row my-6">
            {errors.searchWord && (
              <ErrorBar content={String(errors.searchWord.message)} />
            )}
            {errors.searchCount && (
              <ErrorBar content={String(errors.searchCount.message)} />
            )}
          </div>
        )}
      </>

      {/* DISPLAY RANDOM GIF */}
      <article className="flex items-start mt-3">
        <button
          onClick={getRandomGif}
          className=" font-bold text-actionPurple uppercase text-xs px-3 py-1 border-2 border-actionPurple hover:bg-actionPurple hover:text-white"
        >
          get random gif
        </button>
      </article>
    </section>
  );
};
