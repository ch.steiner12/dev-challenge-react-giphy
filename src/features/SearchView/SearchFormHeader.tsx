import React from "react";
import logo from "../../assets/logos/main_logo.svg";
import heart from "../../assets/icons/heart_filled_white.svg";

export const SearchFormHeader = () => {
  return (
    <>
      <div className="">
        <img src={logo} className=" w-44 md:w-56" />
      </div>
      <div className="flex flex-row gap-2 items-center font-light text-xs">
        <span>made with</span>
        <img src={heart} className="h-4 w-4" />
        <span>by C. Steiner</span>
      </div>
    </>
  );
};
