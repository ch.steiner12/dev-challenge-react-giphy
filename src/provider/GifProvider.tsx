import React, { ReactNode, useEffect, useState } from "react";
import { GifContext } from "../context/GifContext";

type UserProviderProps = {
  children: ReactNode;
};

export const UserProvider = ({ children }: UserProviderProps) => {
  const [favorites, setFavorites] = useState<string[]>([]);

  // check localstorage favorite and set STATE
  useEffect(() => {
    var local = localStorage.getItem("favorites");

    if (local !== `[]`) {
      if (local !== null) {
        const parsedLocal = JSON.parse(local);
        setFavorites(parsedLocal);
      }
    }
  }, []);

  // on state change --> set new LOCALSTORAGE
  useEffect(() => {
    window.localStorage.setItem("favorites", JSON.stringify(favorites));
  }, [favorites]);

  // add Favorite GIF with given id to favorite array
  const addFavoriteGif = (id: string) => {
    setFavorites((prev: any) => [...prev, id]);
  };

  // delete Favorite GIF with given id from favorite array
  const deleteFavoriteGif = (id: string) => {
    if (favorites) {
      const newFavorite = favorites.filter((element) => element !== id);
      setFavorites(newFavorite);
    }
  };

  return (
    <GifContext.Provider
      value={{ favorites, addFavoriteGif, deleteFavoriteGif }}
    >
      {children}
    </GifContext.Provider>
  );
};
