// often used types
export type GifElement = {
  images: { original: { url: string } };
  id: string;
};
