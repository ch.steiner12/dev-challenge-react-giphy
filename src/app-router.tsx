import React from "react";
import { BrowserRouter, Outlet, Route, Routes } from "react-router-dom";
import { FavoritView } from "./features/FavoritView/FavoritView";
import { Layout } from "./features/Layout/Layout";
import { SearchView } from "./features/SearchView/SearchView";

export const AppRouter = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <Layout>
              <Outlet />
            </Layout>
          }
        >
          <Route index element={<SearchView />} />
          <Route path="favorite" element={<FavoritView />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};
