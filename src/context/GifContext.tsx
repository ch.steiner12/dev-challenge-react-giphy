import { createContext } from "react";

interface GifID {
  favorites: string[];
  addFavoriteGif: (id: string) => void;
  deleteFavoriteGif: (id: string) => void;
}

export const GifContext = createContext<GifID>({
  favorites: [],
  addFavoriteGif: () => {},
  deleteFavoriteGif: () => {},
});
