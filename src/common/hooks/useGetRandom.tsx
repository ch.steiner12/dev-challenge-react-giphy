import { useMutation } from "@tanstack/react-query";
import axios from "axios";
import React from "react";

export const useGetRandom = () => {
  return useMutation({
    mutationFn: async () => {
      return axios
        .get(
          `${process.env.REACT_APP_API_URL}/random?api_key=${process.env.REACT_APP_API_KEY}`
        )
        .catch(function (error) {
          throw new Error(error.response.data);
        });
    },
  });
};
