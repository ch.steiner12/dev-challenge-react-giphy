import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import axios from "axios";
import React from "react";
import { SearchWord } from "../../features/SearchView/SearchForm";

export const useGetGif = () => {
  return useMutation({
    mutationFn: async ({ searchWord, searchCount }: SearchWord) => {
      return axios
        .get(
          `${process.env.REACT_APP_API_URL}/search?api_key=${
            process.env.REACT_APP_API_KEY
          }&q=${searchWord}&limit=${searchCount === 0 ? 5 : searchCount}`
        )
        .catch(function (error) {
          throw new Error(error.response.data);
        });
    },
  });
};
