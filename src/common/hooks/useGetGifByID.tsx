import { useQueries } from "@tanstack/react-query";
import axios from "axios";
import React, { useContext } from "react";
import { GifContext } from "../../context/GifContext";

export const useGetGifByID = () => {
  const { favorites } = useContext(GifContext);

  const response = useQueries({
    queries: favorites.map((element) => {
      return {
        queryKey: ["detailGIF", element],
        queryFn: async () => {
          return axios
            .get(
              `${process.env.REACT_APP_API_URL}/${element}?api_key=${process.env.REACT_APP_API_KEY}`
            )
            .catch(function (error) {
              throw new Error(error.response.data);
            });
        },
      };
    }),
  });
  return response;
};
