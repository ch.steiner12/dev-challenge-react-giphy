import React from "react";
import { ThreeDots } from "react-loader-spinner";

export const Loader = () => {
  return (
    <div className="w-full flex justify-center">
      <ThreeDots color="#ffffff" />
    </div>
  );
};
