import React from "react";
import { Link } from "react-router-dom";
import heartIcon from "../../assets/icons/heart_filled_white.svg";
import backIcon from "../../assets/icons/find_icon.svg";

type ActionButtonProps = {
  link: string;
  icon: string;
  title: string;
};

export const ActionButton = ({ link, icon, title }: ActionButtonProps) => {
  return (
    <Link
      to={link}
      className="fade-in-image w-fit hover:no-underline bg-actionPurple text-white px-5 py-2 uppercase font-bold rounded-full text-xs  drop-shadow-lg flex hover:scale-105 hover:transition-all transition-all"
    >
      <div className="flex items-center">
        {icon === "heart" && (
          <img src={heartIcon} className="actionButtonIcon" />
        )}
        {icon === "search" && (
          <img src={backIcon} className="actionButtonIcon" />
        )}
        <span className="hidden ml-0 md:ml-3 md:block">{title}</span>
      </div>
    </Link>
  );
};
